<?php

namespace Database\Seeders;

use App\Models\RakBuku;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('rak_bukus')->delete();

        $data = [
            ['id' => 1, 'rakbuku' => 'Rak 1'],
            ['id' => 2, 'rakbuku' => 'Rak 2'],
            ['id' => 3, 'rakbuku' => 'Rak 3'],
            ['id' => 4, 'rakbuku' => 'rak 4'],
        ];

        foreach ($data as $value) {
            RakBuku::create($value);
        }
    }
}
