<?php

namespace Database\Seeders;

use App\Models\TipeBuku;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('tipe_bukus')->delete();

        $data = [
            ['id' => 1, 'tipebuku' => 'Non-Fiksi'],
            ['id' => 2, 'tipebuku' => 'Fiksi'],
            ['id' => 3, 'tipebuku' => 'Fiksi Ilmiah'],
            ['id' => 4, 'tipebuku' => 'Roman'],
            ['id' => 5, 'tipebuku' => 'Misteri'],
            ['id' => 6, 'tipebuku' => 'Thriller'],
            ['id' => 7, 'tipebuku' => 'Horor'],
        ];

        foreach ($data as $value) {
            TipeBuku::create($value);
        }
    }
}
