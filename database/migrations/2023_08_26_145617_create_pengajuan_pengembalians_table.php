<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengajuan_pengembalians', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->boolean('status')->default(false);
            $table->date('tgl_kembali')->nullable();
            $table->unsignedBigInteger('userbuku_id');
            $table->foreign('userbuku_id')->references('id')->on('user_bukus')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengajuan_pengembalians');
    }
};
