<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tambah_bukus', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->string('nm_buku');
            $table->string('pengarang');
            $table->string('penerbit');
            $table->string('photo_buku',50)->nullable();
            $table->string('tahunterbit');
            $table->unsignedBigInteger('tipe_id');
            $table->foreign('tipe_id')->references('id')->on('tipe_bukus')->onDelete('cascade');
            $table->unsignedBigInteger('rak_id');
            $table->foreign('rak_id')->references('id')->on('rak_bukus')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tambah_bukus');
    }
};
