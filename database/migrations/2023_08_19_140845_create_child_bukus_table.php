<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('child_bukus', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->string('kode_buku');
            $table->boolean('status')->default(false);
            $table->unsignedBigInteger('buku_id');
            $table->foreign('buku_id')->references('id')->on('tambah_bukus')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('child_bukus');
    }
};
