@extends('auth.layouts')

@section('title')
    Detail Buku
@endsection

@section('content')
    {{-- modal 1 --}}
    @foreach ($tambahBooks as $item)
    <div class="modal fade" id="riwayatPeminjam{{ $item->id }}" data-bs-backdrop="static" data-bs-keyboard="false"
        tabindex="-1" aria-labelledby="riwayatPeminjamLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="riwayatPeminjamLabel">Data Peminjam</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email Peminjam</th>
                                <th scope="col">Kode Buku Pinjaman</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $indexCounter = 0;
                            ?>
                            @foreach ($item->childbooks->reverse() as $indexChildbook => $childbook)
                                @foreach ($childbook->userbuku->reverse() as $index => $userBook)
                                    <tr>
                                        <th scope="row">{{ ++$indexCounter }}</th>
                                        <td>{{ $userBook->user->name }}</td>
                                        <td>{{ $userBook->user->email }}</td>
                                        <td>{{ $userBook->buku->kode_buku }}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endforeach


    {{-- modal 2 --}}
    @foreach ($tambahBooks as $item)
        <div class="modal fade" id="childbook{{ $item->id }}" data-bs-backdrop="static" data-bs-keyboard="false"
            tabindex="-1" aria-labelledby="childbookLabel2" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="childbookLabel2">Data Child <br>
                            <a href="{{ route('kirimEmail', ['id'=>$item->id]) }}" class="btn btn-info"><i class="flaticon-381-send"></i></a>
                        </h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Kode Buku</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Barcode</th>
                                    <th scope="col">Nama Buku</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($item->childbooks->isEmpty())
                                    <tr>
                                        <td colspan="5" class="text-center">Data Not Found</td>
                                    </tr>
                                @else
                                    @foreach ($item->childbooks as $index => $childBook)
                                        <tr>
                                            <th scope="row">{{ $index + 1 }}</th>
                                            <td>{{ $childBook->kode_buku }}</td>
                                            <td>
                                                @if ($childBook->status)
                                                    <p class="text-danger fw-bold">Dipinjam</p>
                                                @else
                                                    <p class="text-success fw-bold">Tersedia</p>
                                                @endif
                                            </td>
                                            <td>{!! $childBook->barcode !!}</td>
                                            <td>{{ $childBook->buku->nm_buku }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach




    <!-- row -->
    <div class="container">
        {{-- {{dd($data)}} --}}
        <div class="d-flex flex-wrap">
            @foreach ($tambahBooks as $item)
                <div class="card m-2" style="width: 21rem;">
                    <div class="card-body shadow rounded">
                        <h5 class="card-title">Buku : {{ $item->nm_buku }}</h5>
                        <h6 class="card-subtitle mb-2">Penerbit : {{ $item->penerbit }}</h6>
                        <p class="card-text">Tipe : {{ $item->tipe->tipebuku }}</p>
                        <p class="card-text">Rak : {{ $item->rak->rakbuku }}</p>
                        <div class="d-flex justify-content-between gap-1">
                            <button type="button" class="btn btn-primary w-100" data-bs-toggle="modal"
                                data-bs-target="#riwayatPeminjam{{ $item->id }}">
                                Riwayat Peminjam
                            </button>
                            <button type="button" class="btn btn-info py-0 w-10" data-bs-toggle="modal"
                                data-bs-target="#childbook{{ $item->id }}">
                                <i class="flaticon-381-windows"></i>
                            </button>
                        </div>


                    </div>
                </div>
            @endforeach
        </div>

    </div>
@endsection

@section('js')
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('js/dlabnav-init.js') }}"></script>
@endsection
