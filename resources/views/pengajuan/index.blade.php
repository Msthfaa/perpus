@extends('auth.layouts')

@section('title')
    Pengajuan Pengembalian
@endsection

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">

<style>
    .table {
        border-radius: 12px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); /* Add a subtle shadow */
    }

    .table thead th {
        border-bottom: none; 
        font-weight: bold;
    }

    .table tbody tr:nth-child(odd) {
        background-color: #f7f7f7; 
    }

    .table th,
    .table td {
        padding: 1rem;
    }
</style>

  
<div class="container-fluid">
    <table class="table table-md bg-white">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama <br> Buku</th>
                <th scope="col">Peminjam</th>
                <th scope="col">Kode Buku</th>
                <th scope="col">Tanggal <br> Kembali</th>
                <th scope="col">Acc</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $index => $item)
            <tr>
            <th scope="row">{{ $index + 1 }}</th>
            <td>{{$item->userbuku->buku->buku->nm_buku}}</td>
            <td>{{$item->userbuku->user->name}}</td>
            <td>{{$item->userbuku->buku->kode_buku}}</td>
            <td>{{$item->tgl_kembali}}</td>
            <td>
                @if (!$item->status)         
                <a class="btn btn-success" href="{{ route('pengajuan.acc', ['id'=>$item->id]) }}">
                    <i class="fas fa-check"></i>
                </a>
                    @else
                        <div class="mt-3 text-success text-md">
                            <strong>
                                Berhasil
                            </strong>
                        </div>
                    @endif
                
            </td>
            
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('js')
    @parent
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('js/dlabnav-init.js') }}"></script>
@endsection
