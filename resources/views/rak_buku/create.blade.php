@extends('auth.layouts')

@section('title')
    Tambah Rak Buku
@endsection


@section('content')
        <!-- row -->
        <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Tambah Rak Buku</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form action="{{route('rak_buku.store')}}" method="post"  enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Rak Buku</label>
                                        <input type="text" class="form-control" required placeholder="Rak Buku" name="rakbuku">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                                <button type="button" onclick="history.back()" class="btn btn-danger">Kembali</button>

                            </form>
                        </div>
                    </div>
                </div>

        </div>
    <!--**********************************
        Content body end
    ***********************************-->




</div>

@section('js')

    <script src="{{ asset('js/custom.min.js') }}"></script>
	<script src="{{ asset('js/dlabnav-init.js') }}"></script>

@endsection

@endsection
