@extends('auth.layouts')

@section('title')
    Edit Rak Buku
@endsection


@section('content')
        <!-- row -->
        <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Rak buku</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form action="{{route('rak_buku.update', $data->uuid)}}" method="post"  enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Rak Buku</label>
                                        <input type="text" class="form-control" required placeholder="Rak buku" name="rakbuku" value="{{$data->rakbuku}}">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="button" onclick="history.back()" class="btn btn-danger">Kembali</button>

                            </form>
                        </div>
                    </div>
                </div>
        </div>
    <!--**********************************
        Content body end
    ***********************************-->




</div>

@section('js')

    <script src="{{ asset('js/custom.min.js') }}"></script>
	<script src="{{ asset('js/dlabnav-init.js') }}"></script>

@endsection

@endsection
