@extends('auth.layouts')

@section('title')
    Edit Buku
@endsection


@section('content')
        <!-- row -->
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Buku</h4>
                </div>
                <div class="card-body">
                    <div class="basic-form">
                        <form action="{{ route('tambah_buku.update', $data->uuid) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="mb-3 col-md-12">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">gambar buku</span>
                                        <div class="form-file">
                                            <input type="file" name="photo_buku" accept="image/png, image/jpeg" class="form-file-input form-control">
                                        </div> 
                                    </div>
                                    @if ($data->photo_buku)
                                    <img src="{{ asset('storage/' . $data->photo_buku) }}" alt="Book Cover" style="max-width: 200px;">
                                @endif
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Nama Buku</label>
                                    <input type="text" class="form-control" required value="{{ $data->nm_buku }}" placeholder="Nama Buku" name="nm_buku">
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Pengarang</label>
                                    <input type="text" class="form-control" required value="{{ $data->pengarang }}" placeholder="Pengarang" name="pengarang">
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Penerbit</label>
                                    <input type="text" class="form-control" required value="{{ $data->penerbit }}" placeholder="Penerbit" name="penerbit">
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Batas Peminjama</label>
                                    <input type="number" class="form-control" required value="{{ $data->expired_book }}" placeholder="date" name="expired_book">
                                </div>
                                {{-- <div class="mb-3 col-md-4">
                                    <label class="form-label">Kode Buku</label>
                                    <input type="text" class="form-control" value="{{ $data->kode_buku }}" placeholder="Kode Buku" name="kode_buku" disabled>
                                </div> --}}
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Tahun Terbit</label>
                                    <input type="date" class="form-control" required value="{{ $data->tahun_terbit }}" placeholder="Tahun Terbit" name="tahun_terbit">
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="tipe_id">Tipe Buku</label>
                                    <select class="form-control select2" name="tipe_id">
                                        @foreach($tipebukus as $tipebuku)
                                            <option value="{{ $tipebuku->id }}" {{ $tipebuku->id === $data->tipe_id ? 'selected' : '' }}>
                                                {{ $tipebuku->tipebuku }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="rak_id">Rak Buku</label>
                                    <select class="form-control select2" name="rak_id">
                                        @foreach($rakbukus as $rakbuku)
                                            <option value="{{ $rakbuku->id }}" {{ $rakbuku->id === $data->rak_id ? 'selected' : '' }}>
                                                {{ $rakbuku->rakbuku }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" onclick="history.back()" class="btn btn-danger">Kembali</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!--**********************************
        Content body end
    ***********************************-->




</div>

@section('js')

    <script src="{{ asset('js/custom.min.js') }}"></script>
	<script src="{{ asset('js/dlabnav-init.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/select2.full.min.js') }}"></script>

    <script>
        $('.select2').select2();
    </script>

@endsection

@endsection
