@extends('auth.layouts')

@section('title')
    Data Buku
@endsection

@section('content')
    <!-- Modal -->
<div class="modal fade" id="tb" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Jumlah Buku</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{route('tambah_buku.addqty')}}" method="post">
                @csrf
                <input type="hidden" name="uuid" id="uuid">
                <div class="row">
                    <div class="col-8">
                        <input class="form-control" required name="qty" id="qty" placeholder="Masukan Jumlah Buku Yang Akan Ditambahkan" min="0"/>
                    </input>
                </div>
                <div class="col-2">
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

    <!-- row -->
    <div class="container-fluid">
        @php
            $key = [
                'No' => ['label' => 'angka', 'width' => 50], 
                'Nama Buku' => ['label' => 'nm_buku'],  
                'Tambah Buku' => ['label' => 't_buku'],  
                // 'Jumlah Buku' => ['label' => 'j_buku'],  
                // 'Barcode' => ['label' => 'barcode'],   
    ];
        @endphp
        <x-paginate :data="$data" :key="$key"></x-paginate>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('js/dlabnav-init.js') }}"></script>


    <script>
        function tambah_buku(uuid){
            console.log(uuid);
            $('#uuid').val(uuid);
            $('#qty').val(0);
            $("#tb").modal('show');
        }
    </script>
@endsection
