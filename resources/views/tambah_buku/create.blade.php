@extends('auth.layouts')

@section('title')
    Tambah Buku
@endsection


@section('content')
        <!-- row -->
        <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Tambah Buku</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form action="{{route('tambah_buku.store')}}" method="post"  enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="mb-3 col-md-12">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text">gambar buku</span>
                                            <div class="form-file">
                                                <input type="file" name="photo_buku" accept="image/png, image/jpeg" class="form-file-input form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Nama Buku</label>
                                        <input type="text" class="form-control" required placeholder="Nama Buku" name="nm_buku">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Pengarang</label>
                                        <input type="text" class="form-control" required placeholder="Pengarang" name="pengarang">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Penerbit</label>
                                        <input type="text" class="form-control" required placeholder="Penerbit" name="penerbit">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Tahun Terbit</label>
                                        <input type="date" class="form-control" required placeholder="Tahun Terbit" name="tahun_terbit">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Batas Peminjama</label>
                                        <input type="number" class="form-control" required placeholder="date" name="expired_book">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label" for="tipe_id">Tipe Buku</label>
                                        <select class="form-control select2" name="tipe_id">
                                            <option value="" selected disabled>Pilih</option>
                                            @foreach($tipebukus as $tipebuku)
                                                <option value="{{ $tipebuku->id }}">{{ $tipebuku->tipebuku }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label" for="rak_id">Tipe Buku</label>
                                        <select class="form-control select2" name="rak_id">
                                            <option value="" selected disabled>Pilih</option>
                                            @foreach($rakbukus as $rakbuku)
                                                <option value="{{ $rakbuku->id }}">{{ $rakbuku->rakbuku }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                   
                                   
                                </div>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                                <button type="button" onclick="history.back()" class="btn btn-danger">Kembali</button>

                            </form>
                        </div>
                    </div>
                </div>

        </div>
    <!--**********************************
        Content body end
    ***********************************-->




</div>

@section('js')

    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/select2.full.min.js') }}"></script>
	<script src="{{ asset('js/dlabnav-init.js') }}"></script>

    <script>
        $('.select2').select2();
    </script>

@endsection

@endsection
