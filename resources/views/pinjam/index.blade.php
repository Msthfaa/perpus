@extends('auth.layouts')

@section('title')
    Pinjam Buku
@endsection

@section('content')
    <!-- row -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-4">
                    <div id="reader" width="500px"></div>
                </div>
                <form method="POST" action="{{ route('pinjam.store') }}" class="d-flex align-items-center">
                    @csrf
                    <div class="mb-3 col-9">
                        <label for="result" class="form-label">Filter Kode Buku:</label>
                        <input type="text" class="form-control col-12 w-full" id="result" name="kode_buku">
                    </div>
                    <button type="submit" class="btn btn-primary col-2 align-items-center" style="margin-top: 10px; margin-left: 8px">Pinjam</button>
                </form>

            </div>
        </div>

        {{-- {{dd($data)}} --}}
        <div class="d-flex flex-wrap p-2">
            @foreach ($data->sortByDesc('created_at') as $item)
                <div class="card m-2" style="width: 18rem;">
                    <div class="card-body shadow rounded">
                        <h5 class="card-title">Buku : {{ $item->buku->buku->nm_buku }}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Penerbit : {{ $item->buku->buku->penerbit }}</h6>
                        <p class="card-text">Tipe : {{ $item->buku->buku->tipe->tipebuku }}</p>
                        <p class="card-text">Rak : {{ $item->buku->buku->rak->rakbuku }}</p>
                        <p class="card-text">Kode Buku : {{ $item->buku->kode_buku }}</p>
                        <p class="card-text">
                            Buku harus dikembalikan sebelum tanggal : 
                            @if ($item->expired_date)
                                <strong>
                                    {{ $item->expired_date }}    
                                </strong>
                            @else
                                <p>-</p>
                            @endif
                        </p>
                        

                        @if ($item->buku->status)
                         
                            @if(!isset($item->pengajuan->id))

                            
                            <div>
                                <form action="{{ route('kembalikan.buku', ['id' => $item->id]) }}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <button class="btn btn-danger" type="submit">Kembalikan Buku</button>
                                </form>
                            </div>
                            @else
                            <div class="alert alert-info mt-3 text-warning">
                                <strong>
                                    Pegajuan Pengembalian Diproses Silahakan Datang Keperpus untuk menyerahkan buku
                                </strong>
                            </div>
                            @endif
                        @else
                            <div class="alert alert-info mt-3 text-success">
                                <strong>
                                    Buku sudah dikembalikan
                                </strong>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('js')
    <script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script>
    <script>
        function onScanSuccess(decodedText, decodedResult) {
            // concole.log(`Code matched = ${decodedText}`, decodedResult);
            $('#result').val(decodedText)
        }
        function onScanFailure(error) {
            console.warn(`Code scan error = ${error}`);
        }
        let html5QrcodeScanner = new Html5QrcodeScanner(
            "reader",
            { fps: 10, qrbox: { width: 250, height: 250 } }, false)
        html5QrcodeScanner.render(onScanSuccess, onScanFailure);
    </script>
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('js/dlabnav-init.js') }}"></script>
@endsection
