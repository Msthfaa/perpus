@extends('auth.layouts')

@section('title')
    Tambah Tipe Buku
@endsection


@section('content')
        <!-- row -->
        <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Tambah Tipe Buku</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form action="{{route('tipe_buku.store')}}" method="post"  enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Tipe Buku</label>
                                        <input type="text" class="form-control" required placeholder="Tipe Buku" name="tipebuku">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                                <button type="button" onclick="history.back()" class="btn btn-danger">Kembali</button>

                            </form>
                        </div>
                    </div>
                </div>

        </div>
    <!--**********************************
        Content body end
    ***********************************-->




</div>

@section('js')

    <script src="{{ asset('js/custom.min.js') }}"></script>
	<script src="{{ asset('js/dlabnav-init.js') }}"></script>

@endsection

@endsection
