@extends('auth.layouts')

@section('title')
    Edit tipe Buku
@endsection


@section('content')
        <!-- row -->
        <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit tipe buku</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form action="{{route('tipe_buku.update', $data->uuid)}}" method="post"  enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="mb-3 col-md-6">
                                        <label class="form-label">Tipe Buku</label>
                                        <input type="text" class="form-control" required placeholder="Tipe buku" name="tipebuku" value="{{$data->tipebuku}}">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="button" onclick="history.back()" class="btn btn-danger">Kembali</button>

                            </form>
                        </div>
                    </div>
                </div>
        </div>
    <!--**********************************
        Content body end
    ***********************************-->




</div>

@section('js')

    <script src="{{ asset('js/custom.min.js') }}"></script>
	<script src="{{ asset('js/dlabnav-init.js') }}"></script>

@endsection

@endsection
