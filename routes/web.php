<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginRegisterController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\FaskesController;
use App\Http\Controllers\FaskesVaksinController;
use App\Http\Controllers\KirimEmailController;
use App\Http\Controllers\ProvinsiController;
use App\Http\Controllers\KotaController;
use App\Http\Controllers\PengajuanPengembalianController;
use App\Http\Controllers\PinjamController;
use App\Http\Controllers\RakBukuController;
use App\Http\Controllers\RiwayatController;
use App\Http\Controllers\TambahBukuController;
use App\Http\Controllers\TipeBukuController;
use App\Http\Controllers\VaksinController;
use App\Http\Controllers\UserController;
use App\Models\PengajuanPengembalian;
use App\Models\TambahBuku;
use App\Models\Vaksin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::controller(LoginRegisterController::class)->group(function() {
    Route::get('/register', 'register')->name('register');
    Route::post('/store', 'store')->name('store');
    Route::get('/', 'login')->name('login');
    Route::get('/login', 'login')->name('login');
    Route::post('/authenticate', 'authenticate')->name('authenticate');
    Route::get('/dashboard', 'dashboard')->name('dashboard');
    Route::get('/logout', 'logout')->name('logout');
});
Route::middleware(['auth', 'role:guru'])->group(function () {
    Route::resource('user', UserController::class)->middleware('auth');
    Route::resource('tipe_buku', TipeBukuController::class)->middleware('auth');
    Route::resource('rak_buku', RakBukuController::class)->middleware('auth');
    Route::resource('tambah_buku', TambahBukuController::class)->middleware('auth');
    Route::resource('riwayat', RiwayatController::class)->middleware('auth');
    Route::post('tambah_buku/addqty', [TambahBukuController::class, 'addQty'])->name('tambah_buku.addqty');
    Route::get('/pengajuan/{id}/acc', [PinjamController::class, 'acc'])->name('pengajuan.acc')->middleware('auth');
    Route::get('/kirimEmail/{id}', [KirimEmailController::class, 'index'])->name('kirimEmail');
    Route::resource('pengajuan', PengajuanPengembalianController::class)->middleware('auth');
});

Route::middleware(['auth', 'role:siswa'])->group(function () {
    Route::resource('pinjam', PinjamController::class)->middleware('auth');
    Route::put('/kembalikan_buku/{id}', [PinjamController::class, 'kembalikanBuku'])->name('kembalikan.buku');
});









