<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TambahBuku extends Model
{
    use HasFactory;
    use Uuid;

    protected $fillable = [
        'uuid',
        'nm_buku',
        'pengarang',
        'penerbit',
        'photo_buku',
        'tahun_terbit',
        'expired_book',
        'tipe_id',
        'rak_id',
    ];

    public function tipe(){
        return $this->belongsTo(TipeBuku::class);
    }
    public function rak(){
        return $this->belongsTo(RakBuku::class);
    }

    public function childbooks(){
        return $this->hasMany(ChildBuku::class, 'buku_id', 'id');
    }



}
