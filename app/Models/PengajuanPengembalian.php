<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanPengembalian extends Model
{
    use HasFactory;
    use Uuid;

    protected $fillable = [
        'uuid',
        'status', 
        'tgl_kembali',
        'userbuku_id',
    ];

    public function userbuku(){
        return $this->belongsTo(UserBuku::class);
    }
}
