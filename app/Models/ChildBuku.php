<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildBuku extends Model
{
    use HasFactory;
    use Uuid;

    protected $fillable = ['buku_id', 'kode_buku', 'status'];

    public function buku(){
        return $this->belongsTo(TambahBuku::class, 'buku_id');
    }

    public function userbuku() {
        return $this->hasMany(UserBuku::class, 'buku_id');
        
    }
}
