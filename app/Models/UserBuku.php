<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBuku extends Model
{
    use HasFactory;
    use Uuid;
    

    protected $fillable = ['user_id', 'buku_id', 'expired_date'];

    public function buku(){
        return $this->belongsTo(ChildBuku::class, 'buku_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function pengajuan(){
        return $this->hasOne(PengajuanPengembalian::class, 'userbuku_id', 'id');
    }


}
