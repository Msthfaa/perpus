<?php

namespace App\Http\Controllers;

use App\Mail\KirimEmail;
use App\Models\ChildBuku;
use App\Models\UserBuku;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class KirimEmailController extends Controller
{
    public function index($id){
        $childBuku = ChildBuku::with('userbuku')->where('buku_id', $id)->first();
        foreach ($childBuku->userbuku as $userBuku) {
            if ($userBuku->expired_date == Carbon::now()->format('Y-m-d')) {
                Mail::to($userBuku->user->email)->send(new KirimEmail());  
            }
        }
        return  redirect()->route('dashboard')->withSuccess('Sukses Mengirim Email');
    }
}
