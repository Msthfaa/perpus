<?php

namespace App\Http\Controllers;

use App\Models\ChildBuku;
use App\Models\PengajuanPengembalian;
use App\Models\TambahBuku;
use App\Models\UserBuku;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PinjamController extends Controller
{
    
    public function index(Request $request) {
        $per = (($request->per) ? $request->per : 10);
        $page = (($request->page) ? $request->page-1 : 0);
        DB::statement('set @angka=0+'.$per*$page);
        $data = UserBuku::with(['buku.buku.tipe', 'pengajuan'])->where('user_id', Auth::user()->id)
        ->orderBy('id','asc')
        ->get();
 
        return view('pinjam.index', compact('data'));
    }

    public function kembalikanBuku($id)
    {
        $data = UserBuku::findOrFail($id);
        if (!$data) {
            return redirect()->back()->withErrors('Data tidak ditemukan.');
        }
        $pengajuanBuku = PengajuanPengembalian::create(['userbuku_id'=>$data->id]);

        if ($pengajuanBuku) {
            return redirect()->route('pinjam.index')->withSuccess('Berhasil pengajukan pengembalian buku'.$data->buku->nm_buku.'.');
        } else {
            return redirect()->route('pinjam.index')->withErrors('Gagal mengajukan pengembalian buku.');
        }

    }

    public function acc($id)
    {
        $data = PengajuanPengembalian::with('userbuku.buku')->findOrFail($id);

        $buku = ChildBuku::findOrFail($data->userbuku->buku->id);
        $buku->update(['status' => false]);

        $data->update([
            'status' => true,
            'tgl_kembali' => date('Y-m-d'),
        ]);
        if ($data) {
            return redirect()->route('pengajuan.index')->withSuccess('Buku berhasil dikembalikan.');
        } else {
            return redirect()->route('pengajuan.index')->withErrors('Gagal mengembalikan buku.');
        }
    }

    /**
     * Store the newly created resource in storage.
     */
    public function store(Request $request)
    {
     // dd($request->nm_kota);
     $request->validate([
        'kode_buku' => 'required',
    ]);

    $buku = ChildBuku::with('buku.tipe')->where('kode_buku', $request->kode_buku)->first();

    if(!isset($buku->id)){
        return redirect()->route('pinjam.index')->withErrors('Data Buku Tidak Ada');
    }
    if($buku->status == true){
        return redirect()->route('pinjam.index')->withErrors('Buku Telah dipinjam');
    }

    $buku->update([
        'status' => true
    ]);

    $tambahBuku = TambahBuku::where('id', $buku->buku_id)->first();
    $expiredDate = Carbon::now()->addDays($tambahBuku->expired_book);

    $data = UserBuku::create([
        'user_id' => Auth::user()->id,
        'buku_id' => $buku->id,
        'expired_date' => $expiredDate->format('Y-m-d')
    ]);

    if($data){
        return redirect('pinjam')->withSuccess('Sukses Meminjam Buku '. $buku->buku->nm_buku);
    }

    return redirect()->route('pinjam.create')->withErrors('Sesuatu Error Terjadi');

    }


    /**
     * Display the resource.
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the resource.
     */
    public function edit()
    {
        //
    }

    /**
     * Update the resource in storage.
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the resource from storage.
     */
    public function destroy()
    {
        abort(404);
    }
}
