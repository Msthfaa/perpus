<?php

namespace App\Http\Controllers;

use App\Mail\KirimEmail;
use Illuminate\Support\Facades\Mail;
use App\Models\ChildBuku;
use App\Models\TambahBuku;
use App\Models\UserBuku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Picqer\Barcode\BarcodeGeneratorPNG;


class RiwayatController extends Controller
{
    public function index(Request $request) {
        $per = $request->per ?? 10;
        $page = $request->page ? $request->page - 1 : 0;
    
        $data = UserBuku::with('buku', 'buku.buku.rak', 'buku.buku.tipe')->where('user_id', Auth::user()->id)->orderBy('id', 'asc')->get();
    
        $barcodeGenerator = new BarcodeGeneratorPNG();
        $tambahBooks = TambahBuku::with("tipe", "rak", "childbooks")->get();
    
        foreach ($tambahBooks as $tambahBook) {
            $tambahBook->childbooks->map(function ($childBook) use ($barcodeGenerator) {
                $childBook->barcode = "<img src='data:image/png;base64," . base64_encode($barcodeGenerator->getBarcode($childBook->kode_buku, $barcodeGenerator::TYPE_CODE_128)) . "' width='250'/><br>" . $childBook->kode_buku;
            });
        }
    
        return view('riwayat.index', compact('data', 'tambahBooks'));
    }
    
}
