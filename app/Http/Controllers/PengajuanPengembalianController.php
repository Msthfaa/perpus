<?php

namespace App\Http\Controllers;

use App\Models\PengajuanPengembalian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class PengajuanPengembalianController extends Controller
{
    /**
     * Show the form for creating the resource.
     */
    public function index(Request $request) {
        $per = (($request->per) ? $request->per : 10);
        $page = (($request->page) ? $request->page-1 : 0);
        DB::statement('set @angka=0+'.$per*$page);
        $data = PengajuanPengembalian::with(['userbuku.buku.buku', 'userbuku.user'])
            ->where('id', 'LIKE', '%' . $request->input('search') . '%')
            ->orderBy('id', 'asc')
            ->paginate($per, ['*', DB::raw('@angka  := @angka  + 1 AS angka')]);
 
        return view('pengajuan.index', compact('data'));
    }

    /**
     * Store the newly created resource in storage.
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the resource.
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the resource.
     */
    public function edit()
    {
        //
    }

    /**
     * Update the resource in storage.
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the resource from storage.
     */
    public function destroy()
    {
        abort(404);
    }
}
