<?php

namespace App\Http\Controllers;

use App\Models\ChildBuku;
use App\Models\RakBuku;
use App\Models\TambahBuku;
use App\Models\TipeBuku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Picqer\Barcode\BarcodeGeneratorPNG;

class TambahBukuController extends Controller
{
    public function index(Request $request)
    {
        $per = (($request->per) ? $request->per : 10);
        $page = (($request->page) ? $request->page-1 : 0);
        DB::statement('set @angka=0+'.$per*$page);
        $data = TambahBuku::with('tipe', 'rak')
            ->where('nm_buku', 'LIKE', '%' . $request->input('search') . '%')
            ->orderBy('id', 'asc')
            ->paginate($per, ['*', DB::raw('@angka  := @angka  + 1 AS angka')]);
    
        // Barcode
        // $barcodeGenerator = new BarcodeGeneratorPNG();
        
        $data->map(function($q){
            // $q->barcode .= "<img src='data:image/png;base64,".base64_encode($barcodeGenerator->getBarcode($q->kode_buku, $barcodeGenerator::TYPE_CODE_128))."' width='250'/>";
            $q->t_buku = "<button type='button' class='btn btn-primary' onclick='tambah_buku(`$q->uuid`)'>Tambah Buku</button>";
            return $q;
            $q->aksi = '<a class="btn btn-success btn-sm" href="'.route('faskes.kuota.index', $q->uuid).'">Detail Buku</a>';
            return $q;
        });

        return view('tambah_buku.index', compact('data'));
    }

    public function create()
    {
        $tipebukus = TipeBuku::all();
        $rakbukus = RakBuku::all();
        return view('tambah_buku.create', compact('tipebukus', 'rakbukus'));
    }

    public function addQty(Request $request)
    {
        $request->validate([
            'uuid' => 'required',
            'qty' => 'required',
        ]);

        $buku = TambahBuku::findByUuid($request->uuid);

        if(!isset($buku->id)){
            return redirect()->route('tambah_buku.index')->withErrors('Data Buku Tidak Ada');
        }

        for ($i=0; $i < $request->qty; $i++) { 
            ChildBuku::create([
                'kode_buku' => rand(1, 99999999999),
                'buku_id' => $buku->id,
            ]);
        }
        
        return redirect('tambah_buku')->withSuccess('Sukses Menambah Stok Buku '.$buku->nm_buku.' Sebanyak '.$request->qty);
        
    }

    public function store(Request $request)
    {
        $request->validate([
            'nm_buku' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'expired_book' => 'required',
            'tipe_id' => 'required',
            'rak_id' => 'required',
            'photo_buku' => 'required|mimes:png,jpg,jpeg',
        ]);

        $photoPath = $request->file('photo_buku')->store('images/buku', 'public');

        $nm_buku_abbrev = substr(str_replace(' ', '', strtoupper($request->input('nm_buku'))), 0, 5);
        $date_part = date('YmdHis');
        $kode_buku = $nm_buku_abbrev . $date_part;

        $data = TambahBuku::create([
            'nm_buku' => $request->input('nm_buku'),
            'pengarang' => $request->input('pengarang'),
            'expired_book' => $request->input('expired_book'),
            'penerbit' => $request->input('penerbit'),
            'tipe_id' => $request->input('tipe_id'),
            'rak_id' => $request->input('rak_id'),
            'photo_buku' => $photoPath,
        ]);

        if ($data) {
            return redirect('tambah_buku')->withSuccess('Sukses Menambah Data');
        }

        return redirect()->route('tambah_buku.create')->withErrors('Sesuatu Error Terjadi');
    }

    public function edit(string $id)
    {
        $data = TambahBuku::findByUuid($id);
        $tipebukus = TipeBuku::all();
        $rakbukus = RakBuku::all();
        return view('tambah_buku.edit', compact('data', 'tipebukus', 'rakbukus'));
    }

public function update(Request $request, string $id)
    {
        $request->validate([
            'nm_buku' => 'required',
            'pengarang' => 'required',
            'expired_book' => 'required',
            'penerbit' => 'required',
            'tipe_id' => 'required',
            'rak_id' => 'required',
            'photo_buku' => 'nullable|mimes:png,jpg,jpeg',
        ]);

        $data = TambahBuku::findByUuid($id);

        if ($request->hasFile('photo_buku')) {
            Storage::delete('public/' . $data->photo_buku); 
            $photoPath = $request->file('photo_buku')->store('images/buku', 'public');
        } else {
            $photoPath = $data->photo_buku;
        }

        if ($data->update([
            'nm_buku' => $request->input('nm_buku'),
            'pengarang' => $request->input('pengarang'),
            'expired_book' => $request->input('expired_book'),
            'penerbit' => $request->input('penerbit'),
            'tipe_id' => $request->input('tipe_id'),
            'rak_id' => $request->input('rak_id'),
            'photo_buku' => $photoPath,
        ])) {
            return redirect('tambah_buku');
        }

        return redirect()->route('tambah_buku.edit', $data)->withErrors('Sesuatu Error Terjadi');
    }

public function destroy(string $id)
    {
        $data = TambahBuku::findByUuid($id);

        if (!$data) {
            return redirect()->route('tambah_buku.index')->withErrors('Data Tidak Ada / Sudah Dihapus');
        }

        Storage::delete($data->photo_buku);

        if ($data->delete()) {
            return redirect('tambah_buku')->withSuccess('Sukses Menghapus Data');
        }

        return redirect()->route('tambah_buku.index')->withErrors('Sesuatu Error Terjadi');
    }

    public function pinjam(Request $request)
    {
        $filterKodeBuku = $request->input('kode_buku');
        $dataBuku = TambahBuku::where(function ($query) use ($filterKodeBuku) {
            if ($filterKodeBuku) {
                $query->where('kode_buku', 'LIKE', '%' . $filterKodeBuku . '%');
            }
        })
        ->orderBy('id', 'asc')
        ->paginate(10);
    
        return view('pinjam.index', compact('dataBuku', 'filterKodeBuku'));
    
    }

}
    

