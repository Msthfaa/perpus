<?php

namespace App\Http\Controllers;

use App\Models\RakBuku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RakBukuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $per = (($request->per) ? $request->per : 10);
        $page = (($request->page) ? $request->page-1 : 0);
        DB::statement('set @angka=0+'.$per*$page);
        $data = RakBuku::where(function($q) use ($request) {
            $q->where('rakbuku', 'LIKE', '%'.$request->search.'%');
        })->orderBy('id','asc')->paginate($per, ['*', DB::raw('@angka  := @angka  + 1 AS angka')]);

        return view('rak_buku.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('rak_buku.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'rakbuku' => 'required|string|max:250',
        ]);

        $data = RakBuku::create([
            'rakbuku' => $request->rakbuku,
        ]);

        if($data){
            return redirect()->route('rak_buku.index')->withSuccess('Sukses Menambah data');
        }

        return view('rak_buku.create')->with('error', 'Sesuatu Error Terjadi');


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = RakBuku::get();

        return $data;
    }
    public function get()
    {
        $data = RakBuku::get();
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = RakBuku::findByUuid($id);

        return view('rak_buku.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'rakbuku' => 'required|string|max:250',
        ]);

        $data = RakBuku::findByUuid($id);


        if($data->update([
            'rakbuku' => $request->rakbuku,
        ])){
            return redirect()->route('rak_buku.index')->withSuccess('Sukses Mengubah data');
        }

        return redirect()->route('rak_buku.edit', $data)->withErrors('Sesuatu Error Terjadi');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = RakBuku::findByUuid($id);

        if(!isset($data->id)){
            return redirect()->route('rak_buku.index')->withErrors('Data Tidak Ada / Sudah Dihapus');
        }
        if($data->delete()){
            return redirect()->route('rak_buku.index')->withSuccess('Sukses Menghapus Data');
        }

        return redirect()->route('rak_buku.index')->withErrors('Sesuatu Error Terjadi');
    }
}
